const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const glob = require('glob');
const purifyCSSPlugin = require('purifycss-webpack');

const bootstrapEntryPoints = require('./webpack.bootstrap.config.js');
const isProd = process.env.NODE_ENV === 'production';
const cssDev = ['style-loader','css-loader','sass-loader'];
const cssProd = ExtractTextPlugin.extract({
                fallback : 'style-loader',
                use : [
                'css-loader?minimize=false',
                'sass-loader'
                ],
                publicPath: '/dist/',
                allChunks: true,
            });
const cssConfig = isProd ? cssProd : cssDev;

const bootstrapConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev;

module.exports = {
    entry : {
        index :     './src/js/index.js',
        subpage : './src/js/subpage.js',
        detailpage : './src/js/detailpage.js',
        profilpage : './src/js/profilpage.js',
        productpage : './src/js/productpage.js',
        bootstrap : bootstrapConfig
    },

    output : {
        path: path.resolve(__dirname, "dist"),
        filename: 'js/[name].bundle.js'
    },

    module : {
        rules : [
            {
                test: /\.(sc|c|sa)ss$/,
                use :cssConfig
            },
         
            {
                test : /\.pug$/,
                use : 
                [
                    {
                        loader : 'pug-loader',
                        options : 
                        {
                            pretty : true,
                         }
                    }
                ]
            },
            {
                test : /\.(jpe?g|png|svg|gif)$/i,
                use :
                [
                    'file-loader?name=img/[name].[ext]',
                 //   'file-loader?name=[name].[ext]&outputPath=img/&publicPath=img/',
                    'image-webpack-loader'
                ],
            },
            { 
                test: /\.(woff2?)$/, 
                use: 'url-loader?limit=10000&name=fonts/[name].[ext]' 
            },
            { 
                test: /\.(ttf|eot)$/, 
                use: 'file-loader?name=fonts/[name].[ext]'
            },
            // Bootstrap 3
            { 
                test:/bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/,
                use : 'imports-loader?jQuery=jquery' 
            }
     

        ]
    },
    devServer : {
        hot : false,
        contentBase :path.join(__dirname, 'dist'),
        compress : true,
        port : 8080,
        stats : "errors-only",
        open : true,


    },
    plugins : [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        new HtmlWebpackPlugin({

            title: 'index',
            chunks : ["index","bootstrap"],
            minify : false,
            template: './src/index.html',
        }),
        new HtmlWebpackPlugin({

            title: 'subpage',
            filename: 'subpage.html',
            minify : {
                collapseWhitespace: false,
                preserveLineBreaks: false                
            },
            chunks : ["subpage","bootstrap"],
            template: './src/subpage.html',
        }),
        new HtmlWebpackPlugin({

            title: 'detail page',
            filename: 'detailpage.html',
            minify : {
                collapseWhitespace: false,
                preserveLineBreaks: false                
            },
            chunks : ["detailpage","bootstrap"],
            template: './src/detailpage.html',
        }),
        new HtmlWebpackPlugin({

            title: 'profilpage page',
            filename: 'profilpage.html',
            minify : {
                collapseWhitespace: false,
                preserveLineBreaks: false                
            },
            chunks : ["profilpage","bootstrap"],
            template: './src/profilpage.html',
        }),
        new HtmlWebpackPlugin({

            title: 'PRODUCT page',
            filename: 'productpage.html',
            minify : {
                collapseWhitespace: false,
                preserveLineBreaks: false                
            },
            chunks : ["productpage","bootstrap"],
            template: './src/productpage.html',
        }),
        new ExtractTextPlugin({
            filename: 'css/[name].css',
            disable : !isProd,
            allChunks : true
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),

        //Purify
      //  new purifyCSSPlugin({
        //    paths : glob.sync(path.join(__dirname, 'src/*.html')),
        //})

    ]

}